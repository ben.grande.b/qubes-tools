#!/usr/bin/env python3
"""
Edit Qrexec Policies from any editor. CLI alternative to qubes-policy-editor.
"""

from __future__ import print_function
import argparse
import os
import subprocess
import sys
import tempfile
from qrexec.policy.admin_client import PolicyClient
from qrexec import RPCNAME_ALLOWED_CHARSET
from qrexec.client import VERSION

def get_reply(path, is_include=False):
    """
    Get reply from user.
    """
    print("What now? ", end='')
    reply = str(input())
    if reply == "e":
        lint_policy(path, is_include=is_include)
        return
    if reply == "q":
        sys.exit(0)
    else:
        get_reply(path, is_include=is_include)


def lint_policy(path, is_include=False):
    """
    Open file and lint after closing it. If lint fails, wait for user reply.
    """
    edit_cmd = "${VISUAL:-${EDITOR:-vi}} " + path
    subprocess.run(edit_cmd, shell=True, check=True)

    if is_include:
        lint_cmd = "qubes-policy-lint --include-service " + path
    else:
        lint_cmd = "qubes-policy-lint " + path

    try:
        subprocess.run(lint_cmd, shell=True, check=True)
    except subprocess.CalledProcessError as exc:
        return_code = exc.returncode
        if return_code == 0:
            return
        if return_code == 127:
            print("The linting program 'qubes-policy-lint' is not installed.")
            sys.exit(1)
        else:
            print("Linting failed, do you want to:\n"
                "  (e)dit again\n"
                "  (q)uit without saving changes?")
            get_reply(path, is_include=is_include)


def main(args=None):
    """
    Main.
    """
    if VERSION == "dom0" and os.getuid()!= 0:
        print("You need to run as root in dom0")
        sys.exit(1)

    default_file = "30-user"
    parser = argparse.ArgumentParser()
    parser.add_argument("file", metavar="[include/]FILE",
                        default=default_file,
                        help="Set file to be edited. Will search for an "
                             "editor by looking at $EDITOR, $VISUAL if "
                             "previous entry is unset or 'vi' if previous "
                             "entry is also unset.",
                        nargs="?")
    args = parser.parse_args(args)

    name = args.file
    client = PolicyClient()

    include_prefix = "include/"
    is_include = False
    if name.startswith(include_prefix):
        name = name[len(include_prefix) :]
        is_include = True

    invalid_chars = set(name) - RPCNAME_ALLOWED_CHARSET
    if invalid_chars:
        print("invalid character(s) in the file name: {!r}".format(
            "".join(sorted(invalid_chars)))
        )
        sys.exit(1)

    file_exists = False
    if is_include:
        if name in client.policy_include_list():
            original_content = client.policy_include_get(name)
            file_exists = True
    else:
        if name in client.policy_list():
            original_content = client.policy_get(name)
            file_exists = True

    if file_exists:
        tmpfile = tempfile.NamedTemporaryFile(suffix="_" + name)
        with open(tmpfile.name, 'w', encoding="utf-8") as current_file:
            current_file.write(original_content[0])
            current_file.close()
    else:
        tmpfile = tempfile.NamedTemporaryFile(suffix="_" + name)

    lint_policy(tmpfile.name, is_include=is_include)

    with open(tmpfile.name, "r", encoding="utf-8") as current_file:
        content = current_file.read()
        current_file.close()

    if is_include:
        client.policy_include_replace(name, content)
    else:
        client.policy_replace(name, content)

    tmpfile.close()


if __name__ == "__main__":
    main()
