#!/usr/bin/env python3
"""
Parse Qrexec Policy. Useful to test individual policies.
Paths are not included by the !include* directives on purpose, as we want to
test individual policies.
If the path itself is an included service specified by another policy via
!include-service, then we can lint only files that are services.

You can lint normal policies:
    qubes-policy-lint /etc/qubes/policy.d/*.policy
Or included services:
    qubes-policy-lint -i /etc/qubes/policy.d/include/*
You can't lint both types at the same time.
"""
from __future__ import print_function
import argparse
import sys
from qrexec.exc import PolicySyntaxError
from qrexec.policy.parser import TestPolicy
from qrexec.policy.parser import Rule
# TODO: Update TestPolicy to StringPolicy


def retrieve_list(path):
    """Helper function to retrieve data from given path, or stdin if "-"
    specified, then return it as a list of lines.

    :param path: path or "-"
    :return: list of lines
    """
    if path == "-":
        return [x.rstrip() for x in sys.stdin.readlines()]

    try:
        with open(path, "r", encoding="utf-8") as file:
            return [x.rstrip() for x in file.readlines()]
    except FileNotFoundError:
        msg = "File does not exist."
        print(path + ":0: error: " + msg)
        raise


def parse_file(path, show=False, include_service=False):
    """
    Validate file.
    """
    errors = []
    text = retrieve_list(path)

    for lineno, line in enumerate(text, start=1):
        line = line.strip()

        if not line or line[0] == "#":
            lineno += 1
            continue

        included_path = ''
        if line.startswith("!"):
            directive, *params = line.split()

            if directive == "!include-service":
                if len(params) == 3:
                    # pylint: disable=unused-variable
                    service, argument, included_path = params
            elif directive == "!include":
                if len(params) == 1:
                    (included_path,) = params
            elif directive == "!include-dir":
                # Not implemented upstream, there is no example in
                # qrexec/tests/policy_parser.py
                lineno += 1
                continue

        try:
            # Do not lint included path.
            # But lint itself as an included service if specified by the user.
            if included_path != '':
                if include_service:
                    TestPolicy(policy={'__main__': '!include-service * * inc',
                               'inc': line, included_path: ''})
                else:
                    TestPolicy(policy={'__main__': line, included_path: ''})
            else:
                if include_service:
                    TestPolicy(policy={'__main__': '!include-service * * inc',
                               'inc': line})
                else:
                    TestPolicy(policy={'__main__': line})
        except PolicySyntaxError as exc:
            msg = str(exc).split(":", 2)[-1]
            if show:
                errors.append(path + ":" + str(lineno) +
                              ": error:" + msg + ": " + line)
            else:
                errors.append(path + ":" + str(lineno) +
                              ": error:" + msg)

    if errors:
        print("\n".join(errors))
        sys.exit(1)


def main(args=None):
    """
    Parse arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--show-line", action="store_true",
                        default=False,
                        help="Show the line that caused the error")
    parser.add_argument("-i", "--include-service", action="store_true",
                        default=False,
                        help="Lint a policy that is included by another file "
                             "via !include-service. When this option is "
                             "specified, normal policies cannot be verified")
    parser.add_argument("file", metavar="FILE",
                        help="Set file to be read , use \"-\" to read from "
                             "stdin",
                        nargs="+")
    args = parser.parse_args(args)

    exit_code = 0
    for file in args.file:
        try:
            parse_file(file, show=args.show_line,
                       include_service=args.include_service)
        except PolicySyntaxError:
            exit_code = 1

    if exit_code == 1:
        sys.exit(1)


if __name__ == "__main__":
    main()
